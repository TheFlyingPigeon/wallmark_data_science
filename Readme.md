# Data Science School Project - Store Sales Prediction

## Overview

This project is part of a school assignment aimed at applying data science techniques to understand and predict sales data for 45 stores with 98 different departments. The main objectives of the project are:

- Analyze and gain insights from the provided dataset.
- Conduct feature engineering to prepare the data for modeling.
- Create a predictive model to estimate the markdown and weekly sales for each store and department combination.

## Data

The dataset used in this project contains historical sales data for 45 stores and 98 departments, including information on markdowns, weekly sales, and other relevant features. The dataset is provided as a CSV file and will be used for both exploratory data analysis and model training.

## Project Structure

The project is organized as follows:

- `data/`: Contains the dataset file(s).
- `HTML/`: Contains the notebook as a HTML file.
- `notebooks/`: Jupyter notebooks for data exploration, feature engineering, and model development.
- `results/`: Contains the intermittent results of the project.
- `README.md`: The project documentation you are currently reading.

## Requirements

To run the code and notebooks for this project, you will need the following Python libraries:

- pandas
- numpy
- scikit-learn
- matplotlib
- seaborn
- jupyter

You can install these dependencies using `pip` or `conda`.

## Project Steps

1. **Data Exploration (notebooks/exploratory_data_analysis.ipynb)**

   - Load and explore the dataset to gain insights into the provided data.
   - Visualize key statistics and relationships within the data.
   - Identify any missing values and outliers.

2. **Data Preprocessing (notebooks/data_preprocessing.ipynb, src/data_preprocessing.py)**

   - Handle missing values and outliers.
   - Encode categorical features if necessary.
   - Create additional features using domain knowledge and feature engineering techniques.

3. **Model Development (notebooks/model_training.ipynb)**

   - Split the data into training and testing sets.
   - Train and evaluate machine learning models for predicting markdown and weekly sales.
   - Select the best-performing model based on evaluation metrics such as Mean Squared Error (MSE) or Root Mean Squared Error (RMSE).

4. **Model Evaluation (notebooks/model_evaluation.ipynb)**

   - Assess the model's performance on the test dataset.
   - Visualize model predictions and compare them to the actual sales data.

5. **Conclusion**

   - Summarize the findings from the project.
   - Discuss the model's strengths and weaknesses.
   - Suggest potential improvements and future work.

## Usage

You can follow the provided Jupyter notebooks in the 'notebooks/' directory to step through the project from data exploration to model development. Make sure to adjust file paths and parameters as needed.

## License

This project is licensed under the [MIT License](LICENSE).

Feel free to reach out if you have any questions or need further assistance with the project. Good luck with your data science school project!

